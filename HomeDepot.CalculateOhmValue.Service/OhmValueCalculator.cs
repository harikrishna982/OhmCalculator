﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeDepot.CalculateOhmValue.Service
{
    using ServiceModel;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Base class for ohmvaluecalculator. This is common for band5 and band 6
    /// </summary>
    public class OhmValueCalculator : IOhmValueCalculator
    {
        private IValidationResult validationResult;

        public OhmValueCalculator(IValidationResult validationResult)
        {
            this.validationResult = validationResult;
        }

        /// <summary>
        /// Calculates ohm value
        /// </summary>
        /// <param name="bandAColor"></param>
        /// <param name="bandBColor"></param>
        /// <param name="bandCColor"></param>
        /// <param name="bandDColor"></param>
        /// <returns></returns>
        public virtual decimal CalculateOhmValue(string bandAColor, string bandBColor, string bandCColor, string bandDColor)
        {
            if(!BandAColorValidator(bandAColor))
            {
                validationResult.ErrorMessages.Add("bandAColor", Constants.BandAColorValidationMsg);
            }
            if (!BandBOrCColorValidator(bandBColor))
            {
                validationResult.ErrorMessages.Add("bandBColor", Constants.BandBColorValidationMsg);
            }
            if (!BandBOrCColorValidator(bandCColor))
            {
                validationResult.ErrorMessages.Add("bandCColor", Constants.BandCColorValidationMsg);
            }
            if (!BandDColorValidator(bandDColor))
            {
                validationResult.ErrorMessages.Add("bandDColor", Constants.BandDColorValidationMsg);
            }

            if(!validationResult.IsValid)
            {
                ValidationException ex = new ValidationException();
                foreach(var msg in validationResult.ErrorMessages)
                {
                    ex.Data.Add(msg.Key, msg.Value);
                }
                throw ex;
            }

            return (Constants.BandAColor[bandAColor] * 100 + Constants.BandBOrCColor[bandBColor] * 10 + Constants.BandBOrCColor[bandCColor]) * Constants.BandDColor[bandDColor];


        }

        /// <summary>
        /// Validation for bandA
        /// </summary>
        /// <param name="bandAColor"></param>
        /// <returns></returns>
        protected bool BandAColorValidator(string bandAColor)
        {
            return bandAColor != null && Constants.BandAColor.ContainsKey(bandAColor);
        }

        protected bool BandBOrCColorValidator(string Color)
        {
            return Color != null &&  Constants.BandBOrCColor.ContainsKey(Color);
        }

        protected bool BandDColorValidator(string bandDColor)
        {
            return bandDColor != null &&  Constants.BandDColor.ContainsKey(bandDColor);
        }
    }
}
