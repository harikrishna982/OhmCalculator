﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeDepot.CalculateOhmValue.Service
{
    using ServiceModel;

    public class Band6OhmValueCalculator : OhmValueCalculator
    {
        private IValidationResult validationResult;

        public Band6OhmValueCalculator(IValidationResult validationResult) : base(validationResult)
        {
            this.validationResult = validationResult;
        }
    }
}
