﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeDepot.CalculateOhmValue.ServiceModel
{
    /// <summary>
    /// Validation type in service layer
    /// </summary>
    public interface IValidationResult
    {
        bool IsValid { get; }
        Dictionary<object, object> ErrorMessages { get; set; }
    }
}
