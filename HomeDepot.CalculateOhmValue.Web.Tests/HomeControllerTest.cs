﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;

namespace HomeDepot.CalculateOhmValue.Web.Tests
{
    using AppService;
    using AppServiceModel;
    using Controllers;
    using Service;
    using ServiceModel;
    using Service.Validation;
    using Models;

    [TestClass]
    public class HomeControllerTest
    {
        /// <summary>
        /// to calcualteohm of band 4
        /// </summary>
        [TestMethod]
        public void TestCalculateOhmBand4()
        {

            AppContext appContext = new AppContext();

            ValidationResult valRes = new ValidationResult();

            Func<IAppContext, IOhmValueCalculator> getBand = appcontext => { return new Band4OhmValueCalculator(valRes); };

            var controller = new HomeController(appContext, getBand);

            BandModel bandModel = new BandModel();
            bandModel.bandAColor = "brown";
            bandModel.bandBColor = "black";
            bandModel.bandCColor = string.Empty;
            bandModel.bandDColor = "black";

            var result = controller.CalculateOhm(bandModel) as PartialViewResult;
            var model = (decimal)result.ViewData.Model;
            Assert.AreEqual(10, model);
        }

        [TestMethod]
        public void TestCalculateOhmBand5()
        {

            AppContext appContext = new AppContext();

            ValidationResult valRes = new ValidationResult();

            Func<IAppContext, IOhmValueCalculator> getBand = appcontext => { return new Band5OhmValueCalculator(valRes); };

            var controller = new HomeController(appContext, getBand);

            BandModel bandModel = new BandModel();
            bandModel.bandAColor = "brown";
            bandModel.bandBColor = "black";
            bandModel.bandCColor = "black";
            bandModel.bandDColor = "black";

            var result = controller.CalculateOhm(bandModel) as PartialViewResult;
            var model = (decimal)result.ViewData.Model;
            Assert.AreEqual(100, model);
        }

        [TestMethod]
        public void TestBand5Exception()
        {

            AppContext appContext = new AppContext();

            ValidationResult valRes = new ValidationResult();

            Func<IAppContext, IOhmValueCalculator> getBand = appcontext => { return new Band5OhmValueCalculator(valRes); };

            var controller = new HomeController(appContext, getBand);

            BandModel bandModel = new BandModel();
            bandModel.bandAColor = "brown";
            bandModel.bandBColor = null;
            bandModel.bandCColor = "black";
            bandModel.bandDColor = "black";

            var result = controller.CalculateOhm(bandModel) as PartialViewResult;
            var model = result.ViewData.ModelState["bandBColor"];
            Assert.AreEqual("invalid bandBColor color", model.Errors[0].ErrorMessage);
        }
    }
}
