﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeDepot.CalculateOhmValue.Service.Validation
{
    using ServiceModel;

    /// <summary>
    /// Decoupling service layer validation from client
    /// </summary>
    public class ValidationResult : IValidationResult
    {
        public ValidationResult()
        {
            ErrorMessages = new Dictionary<object, object>();
        }

        public bool IsValid { get { return ErrorMessages.Count == 0; } }
        public Dictionary<object, object> ErrorMessages { get; set; }
    }
}
