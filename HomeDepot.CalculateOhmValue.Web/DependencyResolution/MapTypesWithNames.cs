﻿using StructureMap;
using StructureMap.Graph;
using StructureMap.Graph.Scanning;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeDepot.CalculateOhmValue.Web.DependencyResolution
{
    public class MapTypesWithName<TPlugin> : IRegistrationConvention where TPlugin : class
    {
        public void ScanTypes(TypeSet types, Registry registry)
        {
            types.FindTypes(TypeClassification.Concretes | TypeClassification.Closed).ToList().ForEach(type =>
            {
                if (type == null || type.IsAbstract || !type.IsClass) return;
                if (type.Namespace == null) return;

                var interfaces = type.GetInterfaces();
                var pluginType = typeof(TPlugin);
                if (!interfaces.Any(t => t.IsAssignableFrom(pluginType))) return;
                var specificInterfaceType = interfaces.FirstOrDefault(x => x.GetInterface(pluginType.Name) != null);
                if (specificInterfaceType == null) return;
                var name = type.Name;

                registry.AddType(specificInterfaceType, type, name);
            });
        }
    }
}