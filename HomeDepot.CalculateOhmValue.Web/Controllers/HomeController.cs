﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace HomeDepot.CalculateOhmValue.Web.Controllers
{
    using AppServiceModel;
    using CustomFilters;
    using Models;
    using ServiceModel;
    using Utilities;

    public class HomeController : Controller
    {

        private Func<IAppContext, IOhmValueCalculator> ohmValueCalculator;
        private IAppContext appContext;

        /// <summary>
        /// dependency injection
        /// </summary>
        /// <param name="appContext"></param>
        /// <param name="ohmValueCalculator"></param>
        public HomeController(IAppContext appContext,
            Func<IAppContext, IOhmValueCalculator> ohmValueCalculator)
        {
            this.appContext = appContext;
            this.ohmValueCalculator = ohmValueCalculator;
        }

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Calculate ohm
        /// </summary>
        /// <param name="bandAColor"></param>
        /// <param name="bandBColor"></param>
        /// <param name="bandCColor"></param>
        /// <param name="bandDColor"></param>
        /// <returns></returns>
        [HttpPost]
        [LoggingFilter]
        public ActionResult CalculateOhm(BandModel model)
        {
            //TODO logging
            decimal result;
            try
            {
                result = ohmValueCalculator(appContext).CalculateOhmValue(model.bandAColor, model.bandBColor, model.bandCColor, model.bandDColor);
            }
            catch(ValidationException ex)
            {
                this.ModelState.AddModelErrors(ex);
                return PartialView();
            }

           return PartialView(result);
        }

        /// <summary>
        /// set the band in appcontext
        /// </summary>
        /// <param name="band"></param>
        [HttpPost]
        public void SetBand(string band)
        {
            appContext.Band = band;
        }

    }
}