// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultRegistry.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeDepot.CalculateOhmValue.Web.DependencyResolution {
    using StructureMap;
    using StructureMap.Configuration.DSL;
    using StructureMap.Graph;
    using AppServiceModel;
    using Service;
    using ServiceModel;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AppService;
    using System.Web;
    using System.Web.Mvc;
    using Service.Validation;
    using StructureMap.Pipeline;

    public class DefaultRegistry : Registry {
        #region Constructors and Destructors

        public DefaultRegistry() {
            Scan(
                scan => {
                    scan.AssembliesFromApplicationBaseDirectory();
                    scan.WithDefaultConventions();
					scan.With(new ControllerConvention());
                    scan.Convention<MapTypesWithName<ICoreBehavior>>();
                });

            For<Func<IAppContext, IOhmValueCalculator>>().Use(ctx =>
                 GetBehaviorFunc<IAppContext, IOhmValueCalculator, Band4OhmValueCalculator>(BehaviorPrefixes, typeof(OhmValueCalculator).Name, ctx)
                     ).AlwaysUnique();
        }

        /// <summary>
        /// map appcontext to types
        /// </summary>
        /// <typeparam name="TAppContext"></typeparam>
        /// <typeparam name="TPluginBehavior"></typeparam>
        /// <typeparam name="TDefaultBehaviorInstance"></typeparam>
        /// <param name="instancePrefixes"></param>
        /// <param name="instanceName"></param>
        /// <param name="ctx"></param>
        /// <returns></returns>
        private static Func<TAppContext, TPluginBehavior> GetBehaviorFunc<TAppContext, TPluginBehavior, TDefaultBehaviorInstance>(
  Func<TAppContext, List<string>> instancePrefixes, string instanceName, IContext ctx)
  where TAppContext : class
  where TPluginBehavior : class
  where TDefaultBehaviorInstance : class, TPluginBehavior
        {
            Func<TAppContext, TPluginBehavior> getBehavior =
            applicationContext =>
            {
                TPluginBehavior newInstance = null;

                foreach (var name in instancePrefixes(applicationContext).Select(x => x + instanceName))
                {
                    newInstance = ctx.TryGetInstance<TPluginBehavior>(name);
                    if (newInstance != null) break;
                }

                if (newInstance == null)
                    newInstance = ctx.GetInstance<TDefaultBehaviorInstance>();
                var container =
      new Container(
      c => c.Policies.FillAllPropertiesOfType<TAppContext>().Use(applicationContext));
                container.BuildUp(newInstance);
                return newInstance;
            };
            return getBehavior;
        }

        private static List<string> BehaviorPrefixes(IAppContext appContext)
        {
            return new List<string> { appContext.Band };
        }

        #endregion
    }
}