﻿$(document).ready(function () {

    $(".ajax-form").each(function () {

        $(this).validate({
            rules: {
                bandAColor: "required",
                bandBColor: "required",
                bandCColor: "required",
                bandDColor: "required"
            },
            messages: {
                bandAColor: "Please enter bandAColor",
                bandBColor: "Please enter bandBColor",
                bandCColor: "Please enter bandCColor",
                bandDColor: "Please enter bandDColor",
            }
        });
    });


    $('.ajax-form').submit(function (e) {
        HomeDepotUtilities.formAjaxSubmit(e);
    });

    $('.BandTab').on('click', function (event) {

        $('#tool_output_container').empty();
        $('#errors').empty();

        $.ajax({
            url: '/home/setband',
            type: 'POST',
            data: { 'band': $(event.target).attr('data-band') },
            success: function () {
                console.log('app context set success');
            },
            error: function () {
                console.log('app context set error');
            }
        });
    });
});