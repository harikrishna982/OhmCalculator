﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeDepot.CalculateOhmValue.Web.Models
{
    public class BandModel
    {
        public string bandAColor { get; set; }
        public string bandBColor { get; set; }
        public string bandCColor { get; set; }
        public string bandDColor { get; set; }
    }
}