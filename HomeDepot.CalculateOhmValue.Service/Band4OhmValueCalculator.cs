﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeDepot.CalculateOhmValue.Service
{
    using ServiceModel;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Band 4 ohm value calculator
    /// </summary>
    public class Band4OhmValueCalculator : OhmValueCalculator
    {
        private IValidationResult validationResult;

        public Band4OhmValueCalculator(IValidationResult validationResult) : base(validationResult)
        {
            this.validationResult = validationResult;
        }

        public override decimal CalculateOhmValue(string bandAColor, string bandBColor, string bandCColor, string bandDColor)
        {

            if (!BandAColorValidator(bandAColor))
            {
                validationResult.ErrorMessages.Add("bandAColor", Constants.BandAColorValidationMsg);
            }

            if (!BandBOrCColorValidator(bandBColor))
            {
                validationResult.ErrorMessages.Add("bandBColor", Constants.BandBColorValidationMsg);
            }
           
            if (!BandDColorValidator(bandDColor))
            {
                validationResult.ErrorMessages.Add("bandDColor", Constants.BandDColorValidationMsg);
            }

            if (!validationResult.IsValid)
            {
                ValidationException ex = new ValidationException();
                foreach (var msg in validationResult.ErrorMessages)
                {
                    ex.Data.Add(msg.Key, msg.Value);
                }
                throw ex;
            }

            return (Constants.BandAColor[bandAColor] * 10 + Constants.BandBOrCColor[bandBColor]) * Constants.BandDColor[bandDColor];
        }
    }
}
