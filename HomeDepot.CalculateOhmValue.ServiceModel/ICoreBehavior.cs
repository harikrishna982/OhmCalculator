﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeDepot.CalculateOhmValue.ServiceModel
{
    /// <summary>
    /// interface to filter types while constructing structure map graph. The types which are derived from icorebehaviour are in structure graph
    /// </summary>
    public interface ICoreBehavior
    {
    }
}
