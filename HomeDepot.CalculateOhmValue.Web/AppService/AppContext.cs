﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeDepot.CalculateOhmValue.Web.AppService
{
    using AppServiceModel;

    /// <summary>
    /// Application context: holds application level parameters
    /// </summary>
    public class AppContext : IAppContext
    {
        public string Band
        {
            get
            {
                return Convert.ToString(HttpContext.Current.Session["band"]);
            }
            set
            {
                HttpContext.Current.Session["band"] = value;
            }
        }
    }
}