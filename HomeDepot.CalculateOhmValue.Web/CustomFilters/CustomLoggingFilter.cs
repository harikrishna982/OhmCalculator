﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HomeDepot.CalculateOhmValue.Web.CustomFilters
{
    /// <summary>
    /// Custom logging class
    /// </summary>
    public class LoggingFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
           //TODO log request

            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            //TODO log response
            base.OnActionExecuted(filterContext);
        }
    }
}