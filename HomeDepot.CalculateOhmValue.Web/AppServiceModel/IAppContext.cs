﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeDepot.CalculateOhmValue.Web.AppServiceModel
{
    public interface IAppContext
    {
        string Band { get; set; }
    }
}
